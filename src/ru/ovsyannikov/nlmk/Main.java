package ru.ovsyannikov.nlmk;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final String FILE_NAME = "saveddata.csv";

    public static void main(String[] args) {
        List<Object> persons = new ArrayList<>();
        persons.add(new Person("Tom", "Hawk", LocalDate.of(2018, 3, 9), "hawk@gmail.com"));
        persons.add(new Person("Ben", "Stiller", LocalDate.of(2018, 3, 9)));
        persons.add(new Person("Carl", "Jonson"));

        SerializerListToCsvFile serializerListToCsvFile = new SerializerListToCsvFile();
        serializerListToCsvFile.Serialize(persons, FILE_NAME);
    }

}
