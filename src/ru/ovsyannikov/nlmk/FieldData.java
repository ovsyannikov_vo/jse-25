package ru.ovsyannikov.nlmk;

import java.io.Serializable;

public class FieldData implements Serializable {
    private String fieldName;
    private String className;
    private Object fieldValue;

    public FieldData(String fieldName, String className, Object fieldValue) {
        this.fieldName = fieldName;
        this.className = className;
        this.fieldValue = fieldValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Object getFieldValue() {
        return fieldValue;
    }

    public void setFieldValue(Object fieldValue) {
        this.fieldValue = fieldValue;
    }

    @Override
    public String toString() {
        return "FieldData{" +
                "fieldName='" + fieldName + '\'' +
                ", className='" + className + '\'' +
                ", fieldValue=" + fieldValue +
                '}';
    }

}
