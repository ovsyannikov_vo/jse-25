package ru.ovsyannikov.nlmk;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SerializerListToCsvFile {
    private final DataHandler dataHandler = new DataHandler();
    public boolean Serialize(List<Object> objects, String fileName) {
        Map<String, FieldData> result = new HashMap<>();

        Class objectClass = objects.get(0).getClass();
        for (Object object : objects) {
            for (Field field : objectClass.getDeclaredFields()) {
                field.setAccessible(true);
                try {
                    FieldData fieldData = new FieldData(field.getName(), objectClass.getName(), field.get(object));
                    result.put(objectClass.getName() + "." + field.getName(), fieldData /*+"\r\n"*/);
                    System.out.println(result);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return dataHandler.saveData(result, fileName);
    }

}
